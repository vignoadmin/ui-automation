package com.shaw.StepDefs;

import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.configuration.ConfigurationException;

import com.shaw.framework.FrameworkConstants;
import com.shaw.framework.FrameworkLibrary;
import com.shaw.pages.GlobalConstants;

import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class Hooks extends FrameworkLibrary{

	public Hooks() throws ConfigurationException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Before
	public  void beforeScenario(Scenario scenario) {
		FrameworkConstants.tcResutlsStatusMap.put(scenario.getName() + "_StartTime", String.valueOf(System.currentTimeMillis()));
		GlobalConstants.scenarioName = scenario.getName();
		logger.info("Starting " + GlobalConstants.scenarioName + " scenario");
		System.out.println("Scenario Name Hooks : " + GlobalConstants.scenarioName);
	}

	@After
	public static void afterScenario(Scenario scenario) throws IOException {

		String status = "";
		if (scenario.getStatus().toString().equalsIgnoreCase("passed")) {
			status = "Passed";
			FrameworkConstants.tcResultsMap.put(scenario.getName(), Arrays.asList(status));
			FrameworkConstants.tcResutlsStatusMap.put(scenario.getName(),status);
			
		} else if (scenario.getStatus().toString().equalsIgnoreCase("failed")) {
			status = "Failed";
			FrameworkConstants.tcResultsMap.put(scenario.getName(), Arrays.asList(status, "", ""));
			FrameworkConstants.tcResutlsStatusMap.put(scenario.getName(),status);
		}

		else if (scenario.getStatus().toString().equalsIgnoreCase("skipped")) {
			status = "Skipped";
			FrameworkConstants.tcResultsMap.put(scenario.getName(), Arrays.asList(status, "", ""));
			FrameworkConstants.tcResutlsStatusMap.put(scenario.getName(),status);
		}

		if (FrameworkLibrary.driver != null) {
			if (!FrameworkLibrary.driver.toString().contains("null")) {

				if (scenario.isFailed()) {
					// Take a screenshot...

					String screenshot_path = FrameworkLibrary.takeScreenShot("test");
					String screenshotlink = "<img class='r-img' onerror='this.style.display=\"none\"' data-featherlight="+screenshot_path+" src="+screenshot_path+" data-src="+screenshot_path+">";
					scenario.write(screenshotlink);
				}
				FrameworkLibrary.driver.close();
				FrameworkLibrary.driver.quit();
			}
		}
		FrameworkConstants.tcResutlsStatusMap.put(scenario.getName() + "_EndTime", String.valueOf(System.currentTimeMillis()));
	}
}