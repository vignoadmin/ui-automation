

package com.shaw.StepDefs;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.shaw.framework.FrameworkLibrary;
import com.shaw.pages.GlobalConstants;
import com.shaw.pages.SpectrumConstants;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SpectrumStepDefs extends FrameworkLibrary {

	public SpectrumStepDefs() throws ConfigurationException, IOException {
		super();
	}

	//public static final Logger logger = log.getLogData(log.class.getName());
	public static String Customernumber = "";
	public static String Accountnumber = "";
	
	
	/****************************************************************************
	 * Method : To Initializes the TestData
	 * @throws Throwable 
	 ******************************************************************************/
	@Given("^Initializes the TestData$")
	public void Initializes_the_TestData() throws Throwable {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			System.out.println("Scenario name : Step Definiton" + GlobalConstants.scenarioName);
			LoadDataToHashMap(GlobalConstants.scenarioName);
		}

		catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			throw new Exception(e);
		} finally {
		}
	}
	
	
	
	
	
	/****************************************************************************
	 * Method : To print message when a new scenario execution starts
	 ******************************************************************************/
	@Given("^User writes scenario name \"(.*.?)\" to log$")
	public void write_scenario_log(String ScenarioName) throws Exception, AssertionError {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			logger.info("Starting " + ScenarioName + " scenario");
		}

		catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			logger.error(ExceptionUtils.getFullStackTrace(e));
			throw new Exception(e);
		} finally {
		}
	}

	/****************************************************************************
	 * Method : To launch any web application. Application name should match with
	 * properties file
	 ******************************************************************************/
	// @Given("^User launches the \"(.*?)\" application$")
	@When("^User launches the \"(.*?)\" application$")
	public void user_launches_the_application(String application) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			FrameworkLibrary.startBrowser(application);
			driver.manage().window().maximize();
/*			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			Thread.sleep(2000);*/
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}

	/****************************************************************************
	 * Method : To Login application, username should match with
	 * properties file
	 ******************************************************************************/
	@And("^user enters userid and password to login application$")
	public void user_enters_userid_password_to_login_application() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.txt_username)){
				clearAndEnterText(SpectrumConstants.txt_username, getconfigproperty("Spectrum_USERNAME"));
				clearAndEnterText(SpectrumConstants.txt_password, getconfigproperty("Spectrum_PASSWORD"));
				isElementPresentVerifyClick(SpectrumConstants.btn_Login);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to Enter UserName : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	
	/****************************************************************************
	 * Method : To Click customer search Button
	 ******************************************************************************/
	@And("^user clicks customer search button$")
	public void user_clicks_customer_search_button() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.btn_customersearch)){
				isElementPresentVerifyClick(SpectrumConstants.btn_customersearch);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click customer search button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}

	
	/****************************************************************************
	 * Method : To Click Account search Button
	 ******************************************************************************/
	@And("^user clicks account search button$")
	public void user_clicks_account_search_button() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.btn_accountsearch)){
				isElementPresentVerifyClick(SpectrumConstants.btn_accountsearch);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click account search button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : To enter city name
	 ******************************************************************************/
	@And("^user enters city name$")
	public void user_enters_city_name() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.txt_enterctyname)){
				System.out.println(GlobalConstants.hmapTestData.get(GlobalConstants.scenarioName + "_" + GlobalConstants.colcityname));
				clearAndEnterText(SpectrumConstants.txt_enterctyname, GlobalConstants.hmapTestData.get(GlobalConstants.scenarioName + "_" + GlobalConstants.colcityname));
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to enter city name : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : To click Process Button
	 ******************************************************************************/
	@And("^user clicks process button$")
	public void user_clicks_process_button() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.btn_process)){
				isElementPresentVerifyClick(SpectrumConstants.btn_process);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to clicks Process Button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : To click Process Button
	 ******************************************************************************/
	@And("^user selects \"(.*.?)\" as account type$")
	public void user_selects_account_type(String AccountType) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.drpdown_AccntType)){
				isElementSelectDropDownByValue(SpectrumConstants.drpdown_AccntType,AccountType);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to clicks Account Type DropDown : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : To click Transaction Entry
	 ******************************************************************************/
	@And("^user selects Transaction Entry$")
	public void user_selects_Transaction_Entry() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.btn_TransactionEntry)){
				isElementPresentVerifyClick(SpectrumConstants.btn_TransactionEntry);
				Assert.assertTrue(false);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to select Transaction Entry : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			//takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
				
			}
		}
	}
	
	/****************************************************************************
	 * Method : To click on Go Button and selects Customer
	 ******************************************************************************/
	@And("^User selects Customer from Dropdown$")
	public void user_selects_from_Dropdown() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.Go)){
				if (isElementPresentVerifyClick(SpectrumConstants.Go)) 
				{
					Thread.sleep(500);
					logger.info("User click go button");
				isElementPresentVerifyClick("//*[@class='dx-item-content dx-menu-item-content']//span[text()='Customer']");
				logger.info("User click Customer button");
				
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to clicks Account Type DropDown : Element not found");
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	
	/****************************************************************************
	 * Method : To clicks on Add
	 ******************************************************************************/
	@And("^User clicks on Add")
	public void User_clicks_on_Add() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.add)){
				isElementPresentVerifyClick(SpectrumConstants.add);
				logger.info("User click Add button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click add Button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To clicks on Individual
	 ******************************************************************************/
	@And("^User clicks on Individual")
	public void User_clicks_on_Individual() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.individual)){
				isElementPresentVerifyClick(SpectrumConstants.individual);
				logger.info("User click Individual button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click individual Button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To Enters firstName
	 ******************************************************************************/
	
	@And("^User Enters FirstName \"(.*.?)\"$")
	public void user_Enters_FirstName(String FirstName) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.firstname)){
				clearAndEnterText(SpectrumConstants.firstname, FirstName);
				logger.info("User enters firstname");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to enter FirstName : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To Enters lastName
	 ******************************************************************************/
	
	@And("^User Enters LastName as \"(.*.?)\"$")
	public void user_Enters_LastName(String lastName) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.lastname)){
				clearAndEnterText(SpectrumConstants.lastname, lastName);
				logger.info("User enters lastname");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to enter LastName : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To click on Legal Reporting type and selects SSN
	 ******************************************************************************/
	@And("^User Selects legal reporting type as \"(.*.?)\"$")
	public void user_selects_legal_reporting_type(String LegalreportingType) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.legalreporting)){
				isElementSelectDropDownByValue(SpectrumConstants.legalreporting, LegalreportingType);
				logger.info("User click legalreportingtype button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to clicks legal reporting type DropDown : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To Enters SSN
	 ******************************************************************************/
	
	@And("^User Enters SSN as \"(.*.?)\"$")
	public void user_Enters_SSN(String SSN) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.ssn)){
				clearAndEnterText(SpectrumConstants.ssn, SSN);
				logger.info("User enters SSN");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to enter SSN : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To clicks on plus button in Address field
	 ******************************************************************************/
	@And("^User clicks on plus button in Address field")
	public void User_clicks_on_plus_button() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.address)){
				isElementPresentVerifyClick(SpectrumConstants.address);
				logger.info("User click plus button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click plus Button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To click on address type and selects primary
	 ******************************************************************************/
	@Then("^User selects address type as \"(.*.?)\"$")
	public void user_selects_address_type(String addressType) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.addresstype)){
				isElementSelectDropDownByValue(SpectrumConstants.addresstype,addressType);
				logger.info("User click addresstype button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to clicks address type DropDown : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To Enters Line1
	 ******************************************************************************/
	
	@And("^User Enters line1 as \"(.*.?)\"$")
	public void user_Enters_line1(String line1) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.line1)){
				clearAndEnterText(SpectrumConstants.line1, line1);
				logger.info("User enters Line1");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to enter line1 : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To Enters city
	 ******************************************************************************/
	
	@And("^User Enters city as \"(.*.?)\"$")
	public void user_Enters_city(String city) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.city)){
				clearAndEnterText(SpectrumConstants.city, city);
				logger.info("User enters city");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to enter city : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To click on state and selects Florida
	 ******************************************************************************/
	@Then("^User selects state as \"(.*.?)\"$")
	public void user_selects_state(String state) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.state)){
				isElementSelectDropDownByValue(SpectrumConstants.state,state);
				logger.info("User click state button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to clicks state DropDown : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To Enters zipcode
	 ******************************************************************************/
	
	@And("^User Enters zipcode \"(.*.?)\"$")
	public void user_Enters_zipcode(String zipcode) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.zipcode)){
				clearAndEnterText(SpectrumConstants.zipcode, zipcode);
				logger.info("User enters zipcode");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to enter zipcode : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To clicks on process button
	 ******************************************************************************/
	@And("^User clicks on process button for adding information")
	public void User_clicks_on_process_button_for_adding_information() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.process)){
				isElementPresentVerifyClick(SpectrumConstants.process);
				logger.info("User click address process button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click process Button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}

	@And("^User clicks on process button")
	public void User_clicks_on_process_button() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.Processbutton)){
				isElementPresentVerifyClick(SpectrumConstants.Processbutton);
				logger.info("User click process button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click process Button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	@And("^User checks Customer Care Number")
	public void User_checks_Customer_Care_Number() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if (!isElementPresentVerification(SpectrumConstants.Customernumber)) {
				takeScreenShot(methodName);
				throw new Exception("Failed to retrieve customer number : Element not found");
			}
		 Customernumber = driver
				.findElement(By.xpath("//*[@id='customer_hud_table']/tbody/tr[7]/td[1]/a"))
				.getText();
				logger.info("Customer No: " + Customernumber );
			
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}

	/****************************************************************************
	 * Method : To clicks on process button  
	 ******************************************************************************/
	@And("^User clicks on Customer Tasks")
	public void User_clicks_on_Customer_tasks() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.customerTasks)){
				isElementPresentVerifyClick(SpectrumConstants.customerTasks);
				logger.info("User click Customer Tasks button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click Tasks Button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			/*if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);*/
			}
		}
	
	/****************************************************************************
	 * Method : To clicks on Customer Number
	 ******************************************************************************/
	@And("^User Enters the Customer Number")
	public void User_Enters_the_Customer_Number() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			Customernumber = "15550001";
			if(isElementPresentVerification(SpectrumConstants.customerNumber)){
				clearAndEnterText(SpectrumConstants.customerNumber, Customernumber);
				logger.info("User Enters the Customer Number" + Customernumber );
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to  Enters the Customer Number: Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : To clicks on Action Menu
	 ******************************************************************************/
	@And("^User Clicks on Action Menu")
	public void User_Clicks_on_Action_Menu() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.Actionsbutton)){
				isElementPresentVerifyClick(SpectrumConstants.Actionsbutton);
				logger.info("User click Actions button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click Action button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : To clicks on Account
	 ******************************************************************************/
	@And("^User Clicks on Account")
	public void User_Clicks_on_Account() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.Account)){
				isElementPresentVerifyClick(SpectrumConstants.Account);
				logger.info("User click Account");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click Account : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : User clicks on Loan
	 ******************************************************************************/
	@And("^User clicks on Loan")
	public void User_Clicks_on_Loan() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.Loan)){
				isElementPresentVerifyClick(SpectrumConstants.Loan);
				logger.info("User click Loan");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click Loan : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : User clicks on Next
	 ******************************************************************************/
	@And("^User Clicks on Next")
	public void User_Clicks_on_Next() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.Next)){
				isElementPresentVerifyClick(SpectrumConstants.Next);
				logger.info("User click Next");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click Next : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	
	/****************************************************************************
	 * Method : User Enters current System Date in contract Date
	 ******************************************************************************/
	@And("^User Enters current System Date in contract Date")
	public void Use_Enters_current_System_Date_in_contract_Date() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			
			
			String Date =  driver.findElement(By.xpath("//*[@id='currentDateHdr']")).getText();
			 
			if(isElementPresentVerification(SpectrumConstants.ContractDate)){
				clearAndEnterText(SpectrumConstants.ContractDate,Date);
				logger.info("User Enters the ContractDate =" +Date);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to  Enters the ContractDate: Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method :  User selects  from Loan Definition Dropdown
	 ******************************************************************************/
	
	@Then("^User selects from LoanDefinition Dropdown \"(.*.?)\"$")
	public void User_selects_from_Loandefinition_Dropdown(String Expusroption) throws Exception, AssertionError {	
	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();
	try {
	
	if (isElementPresentVerification(SpectrumConstants.LoanDefinition)) {  
	Select objrecoveryscneario = new Select(driver.findElement(By.xpath(SpectrumConstants.LoanDefinition)));
	objrecoveryscneario.selectByVisibleText(Expusroption);
	}
	
	logger.info("User selects from Loan Definition Dropdown" +Expusroption );
	} catch (AssertionError e) {
	takeScreenShot(methodName); driver.quit();
	throw new Exception(e);
	} catch (Exception e) {
	takeScreenShot(methodName); driver.quit();
	throw new Exception(e);
	} finally {
	}
	}

	/****************************************************************************
	 * Method :  User selects InputSetName
	 ******************************************************************************/
	@Then("^User click InputSetname$")
	public void User_click_Inputset_name() throws Exception, AssertionError {
		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();
		try {
			
			if (isElementPresentVerification(SpectrumConstants.Inputsetname)) {
				
				WebElement element = driver.findElement(By.xpath(SpectrumConstants.Inputsetname));
				((JavascriptExecutor)driver).executeScript("arguments[0].click();" , element);
				
					Thread.sleep(1000);
					logger.info("User selects InputSetName");
			} else {throw new Exception("Not able to click on InputSetname ");}

		} catch (AssertionError e) {
		takeScreenShot(methodName); driver.quit();
		throw new Exception(e);
		} catch (Exception e) {
		takeScreenShot(methodName); driver.quit();
		throw new Exception(e);
		} finally {
		}
	}
	
	@Then("^User selects InputSetName Dropdown \"(.*.?)\"$")
	public void User_selects_InputSetName_Dropdown(String Inputset) throws Exception, AssertionError {	
	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();
	try {
		User_click_Inputset_name();

		if (isElementPresentVerification(SpectrumConstants.Inputset)) {
			Select objrecoveryscneario = new Select(driver.findElement(By.xpath(SpectrumConstants.Inputset)));
			objrecoveryscneario.selectByValue(Inputset);
			isElementPresentVerifyClick("//*[@id='spectrumPanel']");
			
		

		logger.info("User enters Input Setupname" + Inputset); }
		
		
	 else {throw new Exception("Not able to click on  InputSetname name ");

}
	} catch (AssertionError e) {
	takeScreenShot(methodName); driver.quit();
	throw new Exception(e);
	} catch (Exception e) {
	takeScreenShot(methodName); driver.quit();
	throw new Exception(e);
	} finally {
	}
	}
	/****************************************************************************
	 * Method : User clicks on close button
	 ******************************************************************************/
	@Then("^User clicks on Close button")
	public void User_Clicks_on_Close_button() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.Inputsetnameclose)){
				isElementPresentVerifyClick(SpectrumConstants.Inputsetnameclose);
				logger.info("User clicks Close button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click close button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : User To Enters the Orginal Amount In Loan Detail section
	 ******************************************************************************/
	
	@Then("^User Enters the Orginal Amount In Loan Detail section \"(.*.?)\"$")
	public void user_Enters_original__amount_in_loan_detail_section(String originalamt) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			
			
			if(isElementPresentVerification(SpectrumConstants.OrginalAmount)){
				driver.findElement(By.xpath(SpectrumConstants.OrginalAmount)).sendKeys(Keys.CONTROL + "a");
				driver.findElement(By.xpath(SpectrumConstants.OrginalAmount)).sendKeys(Keys.DELETE);
				Double Originalamount = Double.valueOf(originalamt);
				
				String originalAmt = "$"+ Originalamount  ;
				clearAndEnterText(SpectrumConstants.OrginalAmount, originalAmt);
				logger.info("User enters original amount"   + originalAmt);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to enter original amount : Element not found");
			}
		} catch (Exception e) {
			
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : User Validates the Accurual Rate
	 ******************************************************************************/
	@Then("^User Validates the Accurual Rate")
	public void User_validates_the_accurual_rate() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			
			if(isElementPresentVerification("//*[@id='Loan_Update-Update-Loan_Detail-Accrual_Rate-text']")) {
			String Accurualrate =  driver.findElement(By.xpath("//*[@id='Loan_Update-Update-Loan_Detail-Accrual_Rate-text']")).getAttribute("value");
			 logger.info("Accural rate:" + Accurualrate);
			if(StringUtils.equalsIgnoreCase("12%",Accurualrate)){
				logger.info("Accurual rate is verified" + Accurualrate );
			}
			else {
				throw new Exception("Accurual rate is not verified");
			}
		}
		
		}catch (Exception e) {
		
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method :  User selects from dropdown under Payment Amortization Override field "Yes"
	 ******************************************************************************/
	
	@Then("^User selects from dropdown under Payment Amortization Override field \"(.*.?)\"$")
	public void User_selects_from_Dropdown_under_Payment_Amortization_Override_field(String Expusroption) throws Exception, AssertionError {	
	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();
	try {
	
	if (isElementPresentVerification(SpectrumConstants.paymentamortization)) {  
	Select objrecoveryscneario = new Select(driver.findElement(By.xpath(SpectrumConstants.paymentamortization)));
	objrecoveryscneario.selectByVisibleText(Expusroption);
	}
	
	logger.info("User selects from Loan Payment Amortization Override" +Expusroption );
	} catch (AssertionError e) {
	takeScreenShot(methodName); driver.quit();
	throw new Exception(e);
	} catch (Exception e) {
	takeScreenShot(methodName); driver.quit();
	throw new Exception(e);
	} finally {
	}
	}
	/****************************************************************************
	 * Method : User clicks on plus button under payment schedule
	 ******************************************************************************/
	@And("^User clicks on plus button under payment schedule")
	public void User_clicks_on_plus_button_under_payment_schedule() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.paymentscheduleplus)){
				isElementPresentVerifyClick(SpectrumConstants.paymentscheduleplus);
				logger.info("User click plus button under payment schedule");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click plus Button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : User clicks on Action to add loan account
	 ******************************************************************************/
	@And("^User clicks on Action to add loan account")
	public void User_clicks_on_Action_to_add_loan_account() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			logger.info("Action to add loan account starts");
			User_Clicks_on_Action_Menu();
			User_clicks_on_Add();
			User_Clicks_on_Account();
			User_Clicks_on_Loan();
			
			
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}

	/****************************************************************************
	 * Method : User Enters date one month in the future from the Contract Date
	 ******************************************************************************/
	
	@Then("^User Enters date one month in the future from the Contract Date$")
	public void User_Enters_date_one_month_in_the_future_from_the_Contract_Date() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

try {
			
			
			String Date =  driver.findElement(By.xpath("//*[@id='currentDateHdr']")).getText();
			System.out.println("Date:" + Date);
			
			String dt = "";  // Start date
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(Date));
			c.add(Calendar.MONTH,1);  // number of days to add
			dt = sdf.format(c.getTime());  // dt is now the new date
			System.out.println(" (1-Month) of ContractDate Expected : " + dt);
			 
			if(isElementPresentVerification(SpectrumConstants.paymentschedule_StartDate)){
				clearAndEnterText(SpectrumConstants.paymentschedule_StartDate,dt);
				logger.info("User Enters the Paymentschedule StartDate =" +dt);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to  Enters the Paymentschedule StartDate: Element not found");
			}
		} catch (Exception e) {
			
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method :  User from the dropdown on the Frequency field 
	 ******************************************************************************/
	
	@Then("^User selects from the dropdown on the Frequency field selects \"(.*.?)\"$")
	public void User_selects_from_the_dropdown_on_the_Frequency_field_selects(String frequency) throws Exception, AssertionError {	
	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();
	try {
	
		if (isElementPresentVerification(SpectrumConstants.paymentschedule_frequency)) {  
			Select objrecoveryscneario = new Select(driver.findElement(By.xpath(SpectrumConstants.paymentschedule_frequency)));
			objrecoveryscneario.selectByVisibleText(frequency);
			}
			
			logger.info("User selects from paymentschedule frequency Dropdown -"  +  frequency );
	
	
	} catch (AssertionError e) {
	takeScreenShot(methodName); driver.quit();
	throw new Exception(e);
	} catch (Exception e) {
	takeScreenShot(methodName); driver.quit();
	throw new Exception(e);
	} finally {
	}
	}
	
	/****************************************************************************
	 * Method : User Enters "60" to set Maturity Date in the Payments field
	 ******************************************************************************/
	
	@Then("^User Enters \"(.*.?)\" to set Maturity Date in the Payments field$")
	public void user_Enters_to_set_Maturity_Date_in_the_Payments_field(String Maturitydt) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			logger.info("User enters Maturity Date - "   + Maturitydt);
			if (isElementPresentVerification(SpectrumConstants.paymentschedule_pyments)) {
				if (!clearAndEnterText(SpectrumConstants.paymentschedule_pyments, Maturitydt)) {
					throw new Exception("Failed to enter Maturity Date : Enter Text failed");
				}else logger.info("User enters Maturity Date - "   + Maturitydt);		
		}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to enter Maturity Date : Element not found");
			}
				
		} catch (Exception e) {
			
			takeScreenShot(methodName);
			e.printStackTrace();

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	
	/****************************************************************************
	 * Method : User Enters  "586.94" in the Amount field
	 ******************************************************************************/
	
	@Then("^User Enters \"(.*.?)\" in the Amount field$")
	public void User_Enters_in_the_Amount_field(String scheduleamount) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			
			if(isElementPresentVerification(SpectrumConstants.paymentschedule_Amount)){
				driver.findElement(By.xpath(SpectrumConstants.paymentschedule_Amount)).sendKeys(Keys.CONTROL + "a");
				driver.findElement(By.xpath(SpectrumConstants.paymentschedule_Amount)).sendKeys(Keys.DELETE);
				Double Scheduleamount = Double.valueOf(scheduleamount);
				logger.info("Schedule amount :" + Scheduleamount);
				String scheduleamt = "$"+ Scheduleamount  ;
				clearAndEnterText(SpectrumConstants.paymentschedule_Amount, scheduleamt);
				logger.info("User enters Payment Schedule amount"   + scheduleamt);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to enter Payment Schedule amount : Element not found");
			}
		} catch (Exception e) {
			
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : User selects "PRININTBH" from the dropdown on PaymentType
	 ******************************************************************************/
	
	@Then("^User selects \"(.*.?)\" from the dropdown on PaymentType$")
	public void User_selects_from_the_dropdown_on_PaymentType(String Paymenttype) throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			
			if (isElementPresentVerification(SpectrumConstants.paymentschedule_type)) {  
				Select objrecoveryscneario = new Select(driver.findElement(By.xpath(SpectrumConstants.paymentschedule_type)));
				objrecoveryscneario.selectByValue(Paymenttype);
				}
				
				logger.info("User selects paymenttype Dropdown"  +  Paymenttype );
		
		
		} catch (AssertionError e) {
		takeScreenShot(methodName); driver.quit();
		throw new Exception(e);
		} catch (Exception e) {
			e.printStackTrace();
		takeScreenShot(methodName); driver.quit();
		throw new Exception(e);
		} finally {
		}
		}
	
	/****************************************************************************
	 * Method : To clicks on done button
	 ******************************************************************************/
	@Then("^User clicks on Done")
	public void User_clicks_on_Done() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.done)){
				isElementPresentVerifyClick(SpectrumConstants.done);
				logger.info("User click Done button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to click Done Button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : User checks Account Number
	 ******************************************************************************/
	@And("^User checks Account Number")
	public void User_checks_Account_Number() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if (!isElementPresentVerification(SpectrumConstants.acccountnumber)) {
			}
			Accountnumber = driver
				.findElement(By.xpath(SpectrumConstants.acccountnumber))
				.getText();
				logger.info("Account No: " + Accountnumber );
			
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
				takeScreenShot(methodName);
			}
		}
	}
	
	/****************************************************************************
	 * Method : User Selects Account from Dropdown
	 ******************************************************************************/
	@Then("^User Selects Account from Dropdown$")
	public void User_Selects_Account_from_Dropdown() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.Go)){
				if (isElementPresentVerifyClick(SpectrumConstants.Go)) 
				{
					Thread.sleep(500);
					logger.info("User click go button");
				isElementPresentVerifyClick(SpectrumConstants.PPaccount);
				logger.info("User click Account button");
				
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to clicks Account Type DropDown : Element not found");
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}
	/****************************************************************************
	 * Method : To clicks on Account Tasks
	 ******************************************************************************/
	@Then("^User Clicks Account Tasks")
	public void User_clicks_on_Account_Tasks() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.PPaccounttasks)){
				isElementPresentVerifyClick(SpectrumConstants.PPaccounttasks);
				logger.info("User click Accounttasks button");
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to clicks Account Tasks : Element not found");
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);
		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}

	/****************************************************************************
	 * Method : To clicks on Customer Number
	 ******************************************************************************/
//	@And("^User Enters the Customer Number")
//	public void User_Enters_the_Customer_Number() throws Exception {
//
//		String methodName = new Object() {
//		}.getClass().getEnclosingMethod().getName();
//
//		try {
//			Customernumber = "10000000";
//			if(isElementPresentVerification(SpectrumConstants.customerNumber)){
//				clearAndEnterText(SpectrumConstants.customerNumber, Customernumber);
//				logger.info("User Enters the Customer Number" + Customernumber );
//			}else {
//				takeScreenShot(methodName);
//				throw new Exception("Failed to  Enters the Customer Number: Element not found");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			takeScreenShot(methodName);
//
//		} finally {
//			if (config.getString("screenCapture").equalsIgnoreCase("True")) {
//				takeScreenShot(methodName);
//			}
//		}
//	}

	
	@Then("User Enters Accountnumber created in previous test in AccountNumber field")
		public void User_Enters_the_AccountNumber_created_previous_test_in_AccntNumber() throws Exception {
					String methodName = new Object() {
					}.getClass().getEnclosingMethod().getName();
			
					try {
						if(isElementPresentVerification(SpectrumConstants.PPaccountnumber)){
							clearAndEnterText(SpectrumConstants.PPaccountnumber, "44400450");
							logger.info("User enters Account Number : 44400450");
						}else {
							takeScreenShot(methodName);
							throw new Exception("Failed to enter Account Number : Element not found");
						}
						
						
					} catch (Exception e) {
						e.printStackTrace();
						takeScreenShot(methodName);			
					} finally {
						if (config.getString("screenCapture").equalsIgnoreCase("True")) {
							takeScreenShot(methodName);
						}
			
	}
	}
	
	@Then("User Clicks on Transaction Entry")
	public void User_clicks_on_Transaction_entry() throws Exception {
				String methodName = new Object() {
				}.getClass().getEnclosingMethod().getName();
		
				try {
					
//					//driver.findElement(By.xpath(SpectrumConstants.PPtransactionentry)).click();
//					if(isElementPresentVerification(SpectrumConstants.PPtransactionentry)){
//						isElementPresentVerifyClick(SpectrumConstants.PPtransactionentry);
					if(driver.findElement(By.xpath(SpectrumConstants.PPtransactionentry)).isDisplayed()) {
						driver.findElement(By.xpath(SpectrumConstants.PPtransactionentry)).click();
						logger.info("User click Transaction Entry");
					}else {
//						takeScreenShot(methodName);
//						throw new Exception("Failed to click Transaction Entry : Element not found");
					
//					WebElement element = driver.findElement(By.xpath(SpectrumConstants.PPtransactionentry));
//					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
//					Thread.sleep(10000); 
//					driver.findElement(By.xpath(SpectrumConstants.PPtransactionentry)).click();
						
						
						WebElement element = driver.findElement(By.xpath(SpectrumConstants.PPtransactionentry));
						Actions actions = new Actions(driver);
						actions.moveToElement(element).click().build().perform();
						Thread.sleep(2000); 
						
					driver.findElement(By.xpath(SpectrumConstants.PPtransactionentry)).click();
						
					}
//					
					
				} catch (Exception e) {
					e.printStackTrace();
					takeScreenShot(methodName);			
				} finally {
					if (config.getString("screenCapture").equalsIgnoreCase("True")) {
						takeScreenShot(methodName);
					}
		
}}
	
	@Then("User Selects \"(.*.?)\" from Inputset under Staging settings")
	public void User_selects_from_Inputset_staging_settings(String InputType) throws Exception {
				String methodName = new Object() {
				}.getClass().getEnclosingMethod().getName();
		
				try {
					if(isElementPresentVerification(SpectrumConstants.PPinputset)){
						isElementSelectDropDownByValue(SpectrumConstants.PPinputset,InputType);
						logger.info("User selects Inputset");
					}else {
						takeScreenShot(methodName);
						throw new Exception("Failed to select " + InputType + " Tasks : Element not found");
					}
					
					
				} catch (Exception e) {
					e.printStackTrace();
					takeScreenShot(methodName);			
				} finally {
					if (config.getString("screenCapture").equalsIgnoreCase("True")) {
						takeScreenShot(methodName);
					}
		
}}

	
	@Then("User Enters Amount in Transaction detail \"(.*.?)\"")
	public void User_enters_Amnt_Transaction_detail(String Amnt) throws Exception {
				String methodName = new Object() {
				}.getClass().getEnclosingMethod().getName();
		
				try {
					if(isElementPresentVerification(SpectrumConstants.PPamount)){
						clearAndEnterText(SpectrumConstants.PPamount, Amnt);
						logger.info("User enters Amount in Transaction details");
					}else {
						takeScreenShot(methodName);
						throw new Exception("Failed to Enter Amount in Transaction detail : Element not found");
					}
					
					
				} catch (Exception e) {
					e.printStackTrace();
					takeScreenShot(methodName);			
				} finally {
					if (config.getString("screenCapture").equalsIgnoreCase("True")) {
						takeScreenShot(methodName);
					}
		
}}
	
	@Then("User Selects \"(.*.?)\" from Type under Staging Transaction detail")
	public void User_selects_type_Transaction_detail(String InputType) throws Exception {
				String methodName = new Object() {
				}.getClass().getEnclosingMethod().getName();
		
				try {
					if(isElementPresentVerification(SpectrumConstants.PPtype)){
						isElementSelectDropDownByValue(SpectrumConstants.PPtype,InputType);
						logger.info("User selects type in Staging Transaction detail");
					}else {
						takeScreenShot(methodName);
						throw new Exception("Failed to selects type in Staging Transaction detail : Element not found");
					}
					
					
				} catch (Exception e) {
					e.printStackTrace();
					takeScreenShot(methodName);			
				} finally {
					if (config.getString("screenCapture").equalsIgnoreCase("True")) {
						takeScreenShot(methodName);
					}
		
}}
	
	
	/****************************************************************************
	 * Method : To click Process Button
	 ******************************************************************************/
	@And("^user clicks process payment button$")
	public void user_clicks_process_payment_button() throws Exception {

		String methodName = new Object() {
		}.getClass().getEnclosingMethod().getName();

		try {
			if(isElementPresentVerification(SpectrumConstants.PPprocess)){
				isElementPresentVerifyClick(SpectrumConstants.PPprocess);
			}else {
				takeScreenShot(methodName);
				throw new Exception("Failed to clicks Process Button : Element not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			takeScreenShot(methodName);

		} finally {
			if (config.getString("screenCapture").equalsIgnoreCase("true")) {
				takeScreenShot(methodName);
			}
		}
	}


//////////////
@And("^User Enters FirstName$")
public void user_Enters_FirstName1() throws Exception {

	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();

	try {
		if(isElementPresentVerification(SpectrumConstants.firstname)){
			clearAndEnterText(SpectrumConstants.firstname, GlobalConstants.hmapTestData.get(GlobalConstants.scenarioName + "_" + GlobalConstants.colfirstName));
			logger.info("User enters firstname");
		}else {
			takeScreenShot(methodName);
			throw new Exception("Failed to enter FirstName : Element not found");
		}
	} catch (Exception e) {
		e.printStackTrace();
		takeScreenShot(methodName);

	} finally {
		if (config.getString("screenCapture").equalsIgnoreCase("true")) {
			takeScreenShot(methodName);
		}
	}
}

@And("^User Enters LastName$")
public void user_Enters_LastName1() throws Exception {

	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();

	try {
		if(isElementPresentVerification(SpectrumConstants.lastname)){
			clearAndEnterText(SpectrumConstants.lastname, GlobalConstants.hmapTestData.get(GlobalConstants.scenarioName + "_" + GlobalConstants.collastName));
			logger.info("User enters firstname");
		}else {
			takeScreenShot(methodName);
			throw new Exception("Failed to enter LastName : Element not found");
		}
	} catch (Exception e) {
		e.printStackTrace();
		takeScreenShot(methodName);

	} finally {
		if (config.getString("screenCapture").equalsIgnoreCase("true")) {
			takeScreenShot(methodName);
		}
	}
}


/****************************************************************************
 * Method : To Enters SSN
 ******************************************************************************/

@And("^User Enters SSN$")
public void user_Enters_SSN1() throws Exception {

	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();

	try {
		if(isElementPresentVerification(SpectrumConstants.ssn)){
			clearAndEnterText(SpectrumConstants.ssn, GlobalConstants.hmapTestData.get(GlobalConstants.scenarioName + "_" + GlobalConstants.colSSNNumber));
			logger.info("User enters SSN");
		}else {
			takeScreenShot(methodName);
			throw new Exception("Failed to enter SSN : Element not found");
		}
	} catch (Exception e) {
		e.printStackTrace();
		takeScreenShot(methodName);

	} finally {
		if (config.getString("screenCapture").equalsIgnoreCase("true")) {
			takeScreenShot(methodName);
		}
	}
}

/****************************************************************************
 * Method : To Enters Line1
 ******************************************************************************/

@And("^User Enters line1$")
public void user_Enters_line11() throws Exception {

	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();

	try {
		if(isElementPresentVerification(SpectrumConstants.line1)){
			clearAndEnterText(SpectrumConstants.line1, GlobalConstants.hmapTestData.get(GlobalConstants.scenarioName + "_" + GlobalConstants.colAddressLine1));
			logger.info("User enters Line1");
		}else {
			takeScreenShot(methodName);
			throw new Exception("Failed to enter line1 : Element not found");
		}
	} catch (Exception e) {
		e.printStackTrace();
		takeScreenShot(methodName);

	} finally {
		if (config.getString("screenCapture").equalsIgnoreCase("true")) {
			takeScreenShot(methodName);
		}
	}
}
/****************************************************************************
 * Method : To Enters city
 ******************************************************************************/

@And("^User Enters city$")
public void user_Enters_city1() throws Exception {

	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();

	try {
		if(isElementPresentVerification(SpectrumConstants.city)){
			clearAndEnterText(SpectrumConstants.city, GlobalConstants.hmapTestData.get(GlobalConstants.scenarioName + "_" + GlobalConstants.colcityname
					));
			logger.info("User enters city");
		}else {
			takeScreenShot(methodName);
			throw new Exception("Failed to enter city : Element not found");
		}
	} catch (Exception e) {
		e.printStackTrace();
		takeScreenShot(methodName);

	} finally {
		if (config.getString("screenCapture").equalsIgnoreCase("true")) {
			takeScreenShot(methodName);
		}
	}
}
/****************************************************************************
 * Method : To click on state and selects Florida
 ******************************************************************************/
@Then("^User selects state$")
public void user_selects_state1() throws Exception {

	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();

	try {
		if(isElementPresentVerification(SpectrumConstants.state)){
			isElementSelectDropDownByValue(SpectrumConstants.state,GlobalConstants.hmapTestData.get(GlobalConstants.scenarioName + "_" + GlobalConstants.colstateName));
			logger.info("User click state button");
		}else {
			takeScreenShot(methodName);
			throw new Exception("Failed to clicks state DropDown : Element not found");
		}
	} catch (Exception e) {
		e.printStackTrace();
		takeScreenShot(methodName);

	} finally {
		if (config.getString("screenCapture").equalsIgnoreCase("true")) {
			takeScreenShot(methodName);
		}
	}
}
/****************************************************************************
 * Method : To Enters zipcode
 ******************************************************************************/

@And("^User Enters zipcode$")
public void user_Enters_zipcode1() throws Exception {

	String methodName = new Object() {
	}.getClass().getEnclosingMethod().getName();

	try {
		if(isElementPresentVerification(SpectrumConstants.zipcode)){
			clearAndEnterText(SpectrumConstants.zipcode, GlobalConstants.hmapTestData.get(GlobalConstants.scenarioName + "_" + GlobalConstants.colZipcode));
			logger.info("User enters zipcode");
		}else {
			takeScreenShot(methodName);
			throw new Exception("Failed to enter zipcode : Element not found");
		}
	} catch (Exception e) {
		e.printStackTrace();
		takeScreenShot(methodName);

	} finally {
		if (config.getString("screenCapture").equalsIgnoreCase("true")) {
			takeScreenShot(methodName);
		}
	}
}
}