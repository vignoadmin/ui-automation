package com.shaw.wedriver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.shaw.framework.FrameworkLibrary;

public enum WebDriverEnum {
	FIREFOX {
		@Override
		public WebDriver createWebDriver() {
			URL url;
			// url = this.getClass().getResource("/geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver", url.getPath());
			System.setProperty("webdriver.gecko.driver", "src/test/resources/browsers/geckodriver.exe");
			return new FirefoxDriver();
		}
	},
	CHROME {
		@Override
		public WebDriver createWebDriver() {
			URL url;
			if (System.getProperty("os.name").toLowerCase().contains("windows")) {
				url = this.getClass().getResource("/chromedriver.exe");
			} else {
				url = this.getClass().getResource("/chromedriver_linux");
			}
			// System.setProperty("webdriver.chrome.driver", url.getPath());
			System.setProperty("webdriver.chrome.driver", "src/test/resources/browsers/chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("disable-infobars");
			options.addArguments("start-maximized");
			return new ChromeDriver(options);
		}
	},
	INTERNETEXPLORER {
		@Override
		public WebDriver createWebDriver() {
			URL url;
			url = this.getClass().getResource("/IEDriverServer.exe");
			// System.setProperty("webdriver.ie.driver", url.getPath());
			System.setProperty("webdriver.ie.driver", "src/test/resources/browsers/IEDriverServer.exe");
			InternetExplorerOptions options = new InternetExplorerOptions();
			// options.setCapability(capabilityName, value);
			// options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			// options.setCapability(InternetExplorerDriver., value);
			options.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			options.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			options.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			options.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
			// options.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS,true);

			return new InternetExplorerDriver(options);
		}
	},
		REMOTE {
		@Override
		public WebDriver createWebDriver() {
			URL hubUrl = null;
			try {
				hubUrl = new URL("http://localhost:5556/wd/hub");
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			DesiredCapabilities capabilities = DesiredCapabilities.firefox();
			Properties properties = new Properties();
			try {
				properties.load(getClass().getClassLoader().getResourceAsStream("environment.properties"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (System.getProperty("remoteBrowser") == null) {
				capabilities.setBrowserName(properties.getProperty("web.aft.remote.browser"));
			} else {
				capabilities.setBrowserName((System.getProperty("remoteBrowser")));
			}
			capabilities.setPlatform(Platform.WINDOWS);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			return new RemoteWebDriver(hubUrl, capabilities);
		}
	},;

	public abstract WebDriver createWebDriver();

	public static WebDriver getWebDriver(String webDriverType) {
		if (StringUtils.isNotBlank(webDriverType)) {
			WebDriverEnum webDriverEnum = valueOf(StringUtils.upperCase(webDriverType));
			if (webDriverEnum != null) {
				WebDriver webDriver = webDriverEnum.createWebDriver();
				String sessionId=((RemoteWebDriver) webDriver).getSessionId().toString();
				System.out.println("Session Id Created :" + sessionId);
				return webDriver;
			}
		}
		return FIREFOX.createWebDriver();
	}
}