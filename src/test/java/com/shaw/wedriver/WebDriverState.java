package com.shaw.wedriver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

@Component()
@Scope("singleton")
public class WebDriverState {

	@Value("${web.shaw.webdriver.type}")
	private String webDriverType;

	@Value("${web.shaw.webdriver.implicitWait}")
	private long implicitWait;

	private WebDriver webDriver;

	@BeforeClass
	public void create() throws InterruptedException {
		if (webDriver == null) {
			if (System.getProperty("browser") == null) {
				webDriver = WebDriverEnum.getWebDriver(webDriverType);
			} else {
				webDriver = WebDriverEnum.getWebDriver(System.getProperty("browser"));
			}
			webDriver.manage().timeouts().implicitlyWait(implicitWait, TimeUnit.SECONDS);
			webDriver.manage().window().maximize();
			Thread.sleep(1000L);
		}
	}

	@AfterClass
	public void cleanup() {
		webDriver.quit();
	}

	public WebDriver getWebDriver() {
		return webDriver;
	}
}