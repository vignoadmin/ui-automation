package com.shaw.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.shaw.StepDefs.log;
import com.shaw.pages.GlobalConstants;
import com.shaw.wedriver.WebDriverEnum;

import io.cucumber.datatable.DataTable;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class FrameworkLibrary implements ITestListener {

	
	public static String destDir;
	public static DateFormat dateFormat;
	public static ChromeDriver chromeDriver;
	public static WebElement element = null;
	static WebDriverWait browserWithElementWait = null;
	public static WebDriver driver = null;
	static long t1 = 0;
	static long t2 = 0;
	static long timeTaken = 0;
	public static Configuration config = null;
	static Properties prop;
	public static String val = null;
	public static int watingForElement;
	public boolean captureScreenshot;
	public static String propertyname = null;
	Connection conn = null;
	public static String className;
	public static final Logger logger = log.getLogData(log.class.getName());
	
	public FrameworkLibrary() throws ConfigurationException {
		DefaultConfigurationBuilder factory = new DefaultConfigurationBuilder("src/test/resources/config/config.xml");
		config = factory.getConfiguration();
		watingForElement = config.getInt("elementWaitInSecond");
		captureScreenshot = Boolean.valueOf(config.getString("screenCapture"));
	}

	public static void startBrowser(String application) throws IOException {

		String appURL;
		String browser;
		String debugText;

		debugText = "in Start browser for thr app " + application;

		// getSuiteName
		browser = config.getProperty("browser").toString();
		if (config.getProperty("browser." + application) != null) {
			if (config.getProperty("browser." + application).toString() != "")
			{
				browser = config.getProperty("browser." + application).toString();
			}
		}
		appURL = config.getString(config.getProperty("env") + "." + application + ".URL");
		if (application.equals("app1")) {
			driver = WebDriverEnum.getWebDriver(browser);
			appURL = config.getString(config.getProperty("env") + "." + application + ".URL")
					+ FrameworkConstants.sessionID;
			driver.get(appURL);
		} else {
			
			driver = WebDriverEnum.getWebDriver(browser);
			driver.get(appURL);
		}
		
	}

		public static String getconfigproperty(String propKey) {
		if (FrameworkConstants.dbConfigs.containsKey(propKey)) {
			return FrameworkConstants.dbConfigs.get(propKey);
		} else {
			String propoutval = config.getString(propKey);
			return propoutval;
		}
	}

	public static void closeBrowser() {
		driver.close();
		driver.quit();
	}

	public static void linkText_Validation(String objectProperty, String Text) {
		try {
			element = getElementByProperty(objectProperty, driver);
			String linkText = element.getText();
			if (linkText.equalsIgnoreCase(Text)) {
				System.out.println("Link Text expected and actual text are Same");
			} else {
				System.out.println("Link Text expected and actual text are not Same");
				System.out.println("Link Text - Actual : " + linkText);
				System.out.println("Link Text -Expected : " + Text);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

	public static WebElement getElementByProperty(String objectProperty, WebDriver webDriver) {
		String propertyType = null;
		WebDriverWait browserWithElementWait = null;
		try {
			if (browserWithElementWait == null) {
				browserWithElementWait = new WebDriverWait(webDriver, config.getInt("elementWaitInSecond"));
			}
			propertyType = StringUtils.substringAfter(objectProperty, "~");
			objectProperty = StringUtils.substringBefore(objectProperty, "~");
			if (propertyType.equalsIgnoreCase("CSS")) {
				element = browserWithElementWait
						.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(objectProperty)));
				highlightElement(element, webDriver);
			} else if (propertyType.equalsIgnoreCase("XPATH")) {
				element = browserWithElementWait
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath(objectProperty)));
				highlightElement(element, webDriver);
			} else if (propertyType.equalsIgnoreCase("ID")) {
				element = browserWithElementWait
						.until(ExpectedConditions.presenceOfElementLocated(By.id(objectProperty)));
				highlightElement(element, webDriver);
			} else if (propertyType.equalsIgnoreCase("NAME")) {
				element = browserWithElementWait
						.until(ExpectedConditions.presenceOfElementLocated(By.name(objectProperty)));
				highlightElement(element, webDriver);
			} else if (propertyType.equalsIgnoreCase("LINKTEXT")) {
				element = browserWithElementWait
						.until(ExpectedConditions.presenceOfElementLocated(By.linkText(objectProperty)));
				highlightElement(element, webDriver);
			} else {
				element = browserWithElementWait
						.until(ExpectedConditions.presenceOfElementLocated(By.xpath(objectProperty)));
				highlightElement(element, webDriver);
			}
		} catch (Exception e) {

		}

		return element;
	}

	/**
	 * 
	 * will create Screenshot in Screenshot folder and also show filename as
	 * creation date and Time
	 */
	public static String takeScreenShot(String screenshotName) {
		String destFile = "";

		String screenPath = "";
		File targetFile = null;
		try {
			destDir = "target/screenshots";

			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			new File(destDir).mkdirs();
			dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			destFile = dateFormat.format(new Date()) + screenshotName + ".png";

			targetFile = new File(destDir + "/" + destFile);
			if (config.getString("archiveScreenShots") != null
					&& config.getString("archiveScreenShots").equals("true")) {
				File targetShareFile = new File(config.getString("screenShotArchiveShare") + "/"
						+ FrameworkConstants.testRunId + "/" + destFile);
				FileUtils.copyFile(scrFile, targetShareFile);
				screenPath = targetShareFile.getAbsolutePath();
			} else {
				FileUtils.copyFile(scrFile, targetFile);
				screenPath = targetFile.getAbsolutePath();
			}

		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return screenPath;
	}

	public static String getBase64Screenshot() throws IOException {

		String encodedBase64 = null;
		FileInputStream fileInputStream = null;
		TakesScreenshot screenshot = (TakesScreenshot) driver;
		File source = screenshot.getScreenshotAs(OutputType.FILE);
		dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
		String destination = dateFormat.format(new Date()) + "test" + ".gif";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);

		try {
			fileInputStream = new FileInputStream(finalDestination);
			byte[] bytes = new byte[(int) finalDestination.length()];
			fileInputStream.read(bytes);
			encodedBase64 = new String(Base64.encodeBase64(bytes));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return "data:image/gif;base64," + encodedBase64;
	}

	/****************************************************************************
	 * Method : To Verify element present on a page in the application
	 ******************************************************************************/

	public static boolean isElementPresentVerification(String objectProperty) throws AssertionError, Exception {
		boolean isElementPresent = false;
		isElementPresent = FrameworkLibrary.isElementPresents(objectProperty);
		Assert.assertTrue(isElementPresent, "Requested Element with xpath/id: " + objectProperty + " is not visible");
		Thread.sleep(1000);

		return isElementPresent;
	}

	/****************************************************************************
	 * Method : To Verify DropDown exists on a page in the application
	 ******************************************************************************/

	public static void isElementSelectDropDownByValue(String objectProperty,String DropDownText) throws AssertionError, Exception {
		Select dropdown = new Select(driver.findElement(By.xpath(objectProperty)));
		dropdown.selectByValue(DropDownText);
	}
	
		/*
	 * Methods for
	 */

	public static boolean clearAndEnterText(String objectProperty, String Text) {
		boolean isTextEnteredResult = false;
		try {
			//System.out.println(objectProperty);
			//System.out.println("Text Value is=" + Text);
			WebElement textBox = getElementByProperty(objectProperty, driver);
			textBox.clear();
			Thread.sleep(2000);
			//System.out.println("Text box element=" + textBox);
			textBox.sendKeys(Text);
			isTextEnteredResult = true;

		} catch (Exception e) {
			e.printStackTrace();

		}
		return isTextEnteredResult;
	}

	/*
	 * Methods for Highlight the Elements
	 */
	public static void highlightElement(WebElement element, WebDriver webDriver) {
		for (int i = 0; i < 1; i++) {
			JavascriptExecutor js = (JavascriptExecutor) webDriver;
			js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
					"color: red; border: 3px solid red;");
		}
	}

	/*
	 * Methods for entering text using java script
	 */
	public static void enterTextUsingJavaScript(WebDriver webDriver, String locator, String text) {

		for (int i = 0; i < 1; i++) {
			JavascriptExecutor js = (JavascriptExecutor) webDriver;
			js.executeScript("arguments[0].value='" + text + "';", getElementByProperty(locator, webDriver));
		}
	}

	/*
	 * Methods for executing java script
	 */
	public static void executeJavaScript(WebDriver webDriver, String script) {
		for (int i = 0; i < 1; i++) {
			JavascriptExecutor js = (JavascriptExecutor) webDriver;
			js.executeScript(script);
		}
	}

	/*
	 * Methods for
	 */
	public static void browserNavigation_Back() {
		try {
			driver.navigate().back();
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Methods for
	 */
	public static void refresh_Page() {
		chromeDriver.navigate().refresh();
	}

	/*
	 * Common Method for Click
	 */
	public static boolean isElementPresentVerifyClick(String objectProperty) {
		boolean isVerifiedAndClicked = false;
		browserWithElementWait = new WebDriverWait(driver, 30);
		try {
			element = getElementByProperty(objectProperty, driver);
			if (element != null) {
				t1 = System.currentTimeMillis();
				element.click();
				isVerifiedAndClicked = true;
			} else {
				throw new Exception("Object Couldn't be retrieved and clicked");
			}
		} catch (Exception e) {
			element = null;
		}
		return isVerifiedAndClicked;
	}

	public static boolean isElementPresentVerifyClick(String objectProperty, String attribute, String value) {
		boolean isVerifiedAndClicked = false;
		browserWithElementWait = new WebDriverWait(driver, 30);
		int retryAttempt = 0;
		try {
			element = getElementByProperty(objectProperty, driver);
			if (element != null) {
				t1 = System.currentTimeMillis();
				do {
					element.click();
					Thread.sleep(4000);
					retryAttempt++;
				} while (!(element.getAttribute(attribute).contains(value)) && (retryAttempt <= 10));

				if (retryAttempt < 11) {
					isVerifiedAndClicked = true;
				} else {
					throw new Exception("Object Couldn't be clicked");
				}

			} else {
				throw new Exception("Object Couldn't be retrieved and clicked");
			}
		} catch (Exception e) {
			element = null;
		}
		return isVerifiedAndClicked;
	}

	/*
	 * Methods for
	 */

	public static void scrollTo(WebDriver driver, WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
	}

	/*
	 * Methods for
	 */

	public static Map<String, List<String>> getHorizontalData(DataTable dataTable) {
		Map<String, List<String>> dataMap = null;
		try {
			dataMap = new HashMap<String, List<String>>();
			List<String> headingRow = dataTable.row(0);
			int dataTableRowsCount = dataTable.height() - 1;
			ArrayList<String> totalRowCount = new ArrayList<String>();
			totalRowCount.add(Integer.toString(dataTableRowsCount));
			dataMap.put("totalRowCount", totalRowCount);
			for (int i = 0; i < headingRow.size(); i++) {
				List<String> dataList = new ArrayList<String>();
				dataMap.put(headingRow.get(i), dataList);
				for (int j = 1; j <= dataTableRowsCount; j++) {
					List<String> dataRow = dataTable.row(j);
					dataList.add(dataRow.get(i));
				}
			}
		} catch (Exception e) {

		}
		return dataMap;
	}

	/*
	 * Methods for
	 */

	public static Map<String, List<String>> getVerticalData(DataTable dataTable) {
		Map<String, List<String>> dataMap = null;
		try {
			int dataTableRowsCount = dataTable.width();
			dataMap = new HashMap<String, List<String>>();
			for (int k = 0; k < dataTableRowsCount; k++) {
				List<String> dataRow = dataTable.row(k);
				String key = dataRow.get(0);
				dataRow.remove(0);
				dataMap.put(key, dataRow);
			}
		} catch (Exception e) {

		}
		return dataMap;
	}

	/*
	 * Methods for
	 */

	public static String getXLSTestData(String FileName, String SheetName, String RowId, String column)
			throws IOException {
		FileInputStream file = null;
		String col1 = null;
		DataFormatter df = new DataFormatter();
		String environment = config.getString("env");
		String xlsName = FileName + "_" + environment;
		file = new FileInputStream(new File("\\\\networkshare\\testdata" + File.separator + xlsName + ".xls"));

		System.out.println(file);
		HSSFWorkbook book = new HSSFWorkbook(file);
		HSSFSheet sheet = book.getSheet(SheetName);
		int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
		for (int rowIterator = 1; rowIterator <= rowCount; rowIterator++) {
			String row = sheet.getRow(rowIterator).getCell(0).getStringCellValue();
			if (RowId.equalsIgnoreCase(row)) {
				for (int colIterator = 1; colIterator < sheet.getRow(rowIterator).getLastCellNum(); colIterator++) {
					String col = sheet.getRow(0).getCell(colIterator).getStringCellValue();
					if (col.equalsIgnoreCase(column)) {
						Cell cellvalue = sheet.getRow(rowIterator).getCell(colIterator);
						col1 = df.formatCellValue(cellvalue);
						break;

					}
				}
				break;
			}
		}
		// book.close();
		return col1;

	}

	public String getDataFromExcel(String FileName, String SheetName, String column) throws IOException {
		FileInputStream file = null;
		String col1 = null;
		DataFormatter df = new DataFormatter();
		String environment = config.getString("env");
		String xlsName = FileName + "_" + environment;
//		file = new FileInputStream(new File(
//				System.getProperty("user.dir") + "\\src\\test\\resources"+ File.separator + xlsName + ".xls"));
		file = new FileInputStream(new File("\\\\networkshare\\testdata" + File.separator + xlsName + ".xls"));

		System.out.println(file);
		HSSFWorkbook book = new HSSFWorkbook(file);
		HSSFSheet sheet = book.getSheet(SheetName);
		int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
		String contractsList = "";
		for (int rowIterator = 1; rowIterator <= rowCount; rowIterator++) {

			Cell cellvalue = sheet.getRow(rowIterator).getCell(1);
			String row = df.formatCellValue(cellvalue);
			contractsList = contractsList + "'" + row + "'" + ",";
		}
		contractsList = contractsList.replaceAll(",$", "");
		contractsList = "(" + contractsList + ")";
		// book.close();

		return contractsList;

	}

	
	/**
	 * 
	 * Verify text present in a page
	 */
	public static void verifyTextPresent(String objectProperty, String text_to_verify) throws Exception {
		String expected = text_to_verify;
		try {
			browserWithElementWait = new WebDriverWait(driver, watingForElement);
			element = getElementByProperty(objectProperty, driver);
			String actual = element.getText().trim();
			Assert.assertEquals(expected, actual);
		} catch (Exception e) {
			System.out.println("Expected text not matched");
		}
	}

	public static void clearTextField(String objectProperty) {

		WebElement textBox = getElementByProperty(objectProperty, driver);
		textBox.clear();

	}

	public static boolean isButtonEnable(String objectProperty) throws Exception {
		boolean isElementPresent = false;
		browserWithElementWait = new WebDriverWait(driver, watingForElement);
		try {
			element = getElementByProperty(objectProperty, driver);
			if (element != null && element.isEnabled()) {
				isElementPresent = true;
				t2 = System.currentTimeMillis();
			} else {
				isElementPresent = false;
			}
			Thread.sleep(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isElementPresent;
	}


	public static void enter_Text(DataTable arg1, int rowNum, String colval, String propval, WebDriver driver)
			throws Exception {
		try {
			Map<String, List<String>> dataMap = null;
			dataMap = FrameworkLibrary.getHorizontalData(arg1);
			val = getXLSTestData(dataMap.get("InputFileName").get(rowNum), dataMap.get("SheetName").get(rowNum),
					dataMap.get("RowId").get(rowNum), colval);
			System.out.println(val);
			// WebElement ele = chromeDriver.findElementById(propval);
			if (!clearAndEnterText(propval, val)) {
				throw new Exception("User Not able to Enter" + val + "in TextField");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String getData(DataTable arg1, String colval, String propval, WebDriver driver) throws IOException {

		Map<String, List<String>> dataMap = null;
		dataMap = FrameworkLibrary.getHorizontalData(arg1);
		val = getXLSTestData(dataMap.get("InputFileName").get(0), dataMap.get("SheetName").get(0),
				dataMap.get("RowId").get(0), colval);
		System.out.println(val);
		return val;
	}

	public static boolean isElementPresents(String xPath) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, watingForElement);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
			driver.findElement(By.xpath(xPath));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static String getElementText(String xPath) {
		WebElement property = FrameworkLibrary.getElementByProperty(xPath, driver);
		if (property.getText() != null && !property.getText().isEmpty()) {
			System.out.println("Element Text is : " + property.getText());
			return property.getText();
		} else {
			System.out.println("Element value is : " + property.getAttribute("value"));
			return property.getAttribute("value");
		}
	}


	public static String WaitforElement(String element) {
		// Explicit wait
		WebDriverWait wait = new WebDriverWait(chromeDriver, watingForElement);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(element)));
		return element;

	}

	public static void TextAssertion(String objectproperty, String expectedresult) {
		String s = FrameworkLibrary.getElementText(objectproperty);
		try {
			Assert.assertEquals(s, expectedresult);
		} catch (AssertionError e) {
			System.out.println("Assertion error");
		}
		System.out.println("Assertion is done");

	}

	public static void waitForVisibility(String xpath) {
		WebDriverWait wait = new WebDriverWait(driver, watingForElement);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
	}

	public static WebElement waitToClick(String xpath) {
		WebDriverWait waitToClick = new WebDriverWait(driver, watingForElement);
		WebElement element = waitToClick.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		return element;
	}

	public static boolean isElementPresentbyClassname(String className) {
		try {
			WebDriverWait wait = new WebDriverWait(chromeDriver, watingForElement);
			wait.until(ExpectedConditions.visibilityOf(chromeDriver.findElement(By.className(className))));

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public static WebElement waitToClickByClassName(String className) {
		WebDriverWait waitToClick = new WebDriverWait(chromeDriver, watingForElement);
		WebElement element = waitToClick.until(ExpectedConditions.elementToBeClickable(By.className(className)));
		return element;
	}

	
	/***
	 * @author skommera
	 * @Description : To Load TestData from Excel
	 */
	public void LoadDataToHashMap(String scenarioName) throws Throwable{
		try {
			InputStream ExcelPath = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/TestData/TestData.xlsx");
			XSSFWorkbook wb = new XSSFWorkbook(ExcelPath);
			XSSFSheet sheet = wb.getSheet(getconfigproperty("env"));
			System.out.println("sheet Rows : " + sheet.getLastRowNum());
			/** Iterate over rows in Sheet**/
			loop:
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				if(StringUtils.equals(scenarioName, getCellValueString(sheet.getRow(i).getCell(0)))) {
					/** Iterate over columns based on Header Row and Add the data to HashMap **/
					for (int j = 1; j < sheet.getRow(0).getLastCellNum(); j++) {
						GlobalConstants.hmapTestData.put(scenarioName + "_" + getCellValueString(sheet.getRow(0).getCell(j)), getCellValueString(sheet.getRow(i).getCell(j)));
						//break loop;
					}
					System.out.println(GlobalConstants.hmapTestData.toString());
						wb.close();
						break loop;
				}
				
//				else if(i == sheet.getLastRowNum()) {
//					logger.error("Scenario Name not found in Excel sheet");
//					throw new Exception("Exit Test. Scenario Name not found in Excel sheet");
//				}
			}
			
			
		}catch(Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			throw new Exception(e);
			
		}
	}
	
	
	 public static String getCellValueString(Cell cell) {
	        String value="";
	        if(cell!=null) {
	            switch(cell.getCellType()){
	                case Cell.CELL_TYPE_BOOLEAN:
	                    value=String.valueOf(cell.getBooleanCellValue());
	                    break;
	                case Cell.CELL_TYPE_NUMERIC:
	                    value=BigDecimal.valueOf(
	                        cell.getNumericCellValue()).toPlainString();
	                    break;
	                case Cell.CELL_TYPE_STRING:
	                    value=String.valueOf(cell.getStringCellValue());
	                    break;
	                case Cell.CELL_TYPE_FORMULA:
	                    value=String.valueOf(cell.getCellFormula());
	                    break;
	                case Cell.CELL_TYPE_BLANK:
	                    value="";
	                    break;
	            }
	        } else {
	        	System.out.println("Cell value is Null");
	        }
	        return value.trim();
	    }
	 
	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		className = context.getCurrentXmlTest().getClasses().get(0).getName();
		className = className.substring(className.lastIndexOf(".") + 1);
		System.out.println(className);

	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub

	}

}
