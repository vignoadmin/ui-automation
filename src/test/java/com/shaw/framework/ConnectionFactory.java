package com.shaw.framework;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public class ConnectionFactory {

	private static ConnectionFactory instance = new ConnectionFactory();
	public static final String DRIVER_CLASS = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	public static String Environment = FrameworkLibrary.config.getString("env");
    
	
	private ConnectionFactory() {
		try {
			Class.forName(DRIVER_CLASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public Connection createConnection() {
		
		Connection connection = null;
		try {
			
			System.out.println("connecting to" +Environment+ "database");
			String url = FrameworkLibrary.config.getString(Environment+"_URL");
			System.out.println("url is"+url);
			String user = FrameworkLibrary.config.getString(Environment+"_USER");
			System.out.println("user is"+user);
			String password=FrameworkLibrary.config.getString(Environment+"_PASSWORD");
			System.out.println("password is"+password);
			
			connection = DriverManager.getConnection(url, user, password);

		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("ERROR: Unable to Connect to Database.");
		}
		return connection;
	}	
	
	public static Connection getConnection() {
		return instance.createConnection();
	} 
	
public Connection createADIntegratedConnection(String serverName,String schema) {
		
		Connection connection = null;
		try {
			
			 SQLServerDataSource ds = new SQLServerDataSource();
	        ds.setServerName(serverName); // Replace with your server name
	        ds.setDatabaseName(schema); // Replace with your database name
	        ds.setAuthentication("ActiveDirectoryIntegrated");
	       ds.setHostNameInCertificate("*.database.windows.net");
	       connection = ds.getConnection();
	       }
		catch (SQLException e) {
			e.printStackTrace();
			System.out.println("ERROR: Unable to Connect to Database.");
		}
		return connection;
	}	
	
public static Connection getADIntegratedConnection(String serverName,String schema) {
	return instance.createADIntegratedConnection(serverName,schema);
} 
	


public Connection createWindowsAuthenticatedConnection(String serverName,String schema) {
	
	Connection connection = null;
	try {
		
		 SQLServerDataSource ds = new SQLServerDataSource();
	        ds.setServerName(serverName); // Replace with your server name
	        ds.setDatabaseName(schema); // Replace with your database name
	        ds.setIntegratedSecurity(true);
     
       connection = ds.getConnection();
       }
	catch (SQLException e) {
		e.printStackTrace();
		System.out.println("ERROR: Unable to Connect to Database.");
	}
	return connection;
}	

public static Connection getWindowsAuthenticatedConnection(String serverName,String schema) {
return instance.createWindowsAuthenticatedConnection(serverName,schema);
}
	
}