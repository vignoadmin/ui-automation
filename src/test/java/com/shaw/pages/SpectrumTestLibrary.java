package com.shaw.pages;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.shaw.framework.FrameworkLibrary;

import io.cucumber.datatable.DataTable;

public class SpectrumTestLibrary extends FrameworkLibrary {
	
	public static WebDriverWait wait;
	public SpectrumTestLibrary() throws ConfigurationException, IOException {
		super();
	}
	public static void enter_Text(DataTable arg1, String colval, String propval, WebDriver driver) throws Exception
	{
		try
		{
			Map<String, List<String>> dataMap = null;
			dataMap = FrameworkLibrary.getHorizontalData(arg1);
			val = getXLSTestData(dataMap.get("InputFileName").get(0), dataMap.get("SheetName").get(0), dataMap.get("RowId").get(0), colval);
			System.out.println(val);
			if(!clearAndEnterText(propval,val)) {
				throw new Exception("User Not able to Enter" +val+ "in TextField");
			}
		}
		catch (IOException e) {
			e.printStackTrace();
	}
		
	}
	
  /*This method is used to get the data from excel sheet and store it into a string.
	It picks the data from excel sheet based on the file name, sheet name and Row Id given in the DataTable.*/
	
  public static String getData(DataTable arg1,String colval) throws IOException{ 
	  Map<String, List<String>> dataMap = null;
		dataMap = FrameworkLibrary.getHorizontalData(arg1);
		String value = getXLSTestData(dataMap.get("InputFileName").get(0), dataMap.get("SheetName").get(0), dataMap.get("RowId").get(0), colval);
		System.out.println(value);
		return value;  
  }

	public static WebElement waitToClick(String xpath){
		wait = new WebDriverWait(chromeDriver,60);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
		return element;	
	}
	


}