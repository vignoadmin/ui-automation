package com.shaw.pages;
public class SpectrumConstants {
	
	
	
	//Add customer
	
	public static final String btn_Login = "//*[@id='loginButton']";
	public static final String txt_username = "//*[@id='userId']";
	public static final String txt_password = "//*[@id='password']";
	public static final String btn_customersearch = "//*[@id='customer_search_link']/div[2]";
	public static final String btn_accountsearch = "//*[@id='account_search_link']/div[2]";
	public static final String btn_TransactionEntry = "//*[@id='transactions_link']/div[2]";
	public static final String txt_enterctyname = "//*[@id='Customer_Tasks-Search-Customer_Search_Criteria-City-text']";
	public static final String btn_process = "//*[@id='buttons_bottom']/span/a";
	public static final String drpdown_AccntType = "//*[@id='Account_Tasks-Search-Account_Search_Criteria-Account_Type-select']";
	public static final String Go = "//*[@id=\'btnMain\']/div";
	public static final String customer = "//*[@id=\'dx-09bb21d6-1bf7-aed4-3c91-692ad26d1141\']/div/ul/li[2]/div/div[1]/span[1]";
	public static final String add = "//*[@class='dx-item-content dx-menu-item-content']//span[text()='Add']";
	public static final String individual = "//*[@class='dx-item-content dx-menu-item-content']//span[text()='Individual']";
	public static final String firstname = "//*[@id=\'Individual_Customer_Add-Individual_Customer_Add-Name-First-text\']";
	public static final String lastname = "//*[@id=\'Individual_Customer_Add-Individual_Customer_Add-Name-Last-text\']";
	public static final String legalreporting ="//*[@id=\'Individual_Customer_Add-Individual_Customer_Add-Identity-Legal_Reporting_Type-select\']";
	public static final String ssn = "//*[@id=\"Individual_Customer_Add-Individual_Customer_Add-Identity-SSN-text\"]";
	public static final String address = "//*[@id=\'Addresses_dynaAdd\']";
	public static final String Customernumber = "//*[@id='customer_hud_table']/tbody/tr[7]/td[1]/a"; 
	public static final String Processbutton = "//*[@id='buttons_top']/span/a";
	
	//address pop-up
	public static final String addresstype = "//*[@id='Add_Address-Add-Address-Address_Type-select']";
	public static final String line1 = "//*[@id='event.customer.addresses[0].streetAddressLine1']";
	public static final String city = "//*[@id='event.customer.addresses[0].city']";
	public static final String state = "//*[@id='Add_Address-Add-Address-State-select']";
	public static final String zipcode = "//*[@id='event.customer.addresses[0].postalCode']";
	public static final String process = "//*[@id='addCatWin_content']/div[2]/div";
	
	
	//AddingloantoCustomer
	
	public static final String customerTasks = "//*[@class='dx-item-content dx-menu-item-content']//span[text()='Customer Tasks']";
	public static final String customerNumber ="//*[@id='Customer_Tasks-Retrieve-Customer_Retrieve-Customer_Number-text']";
	public static final String Process ="//*[@id='buttons_top']/span/a";
	public static final String Actionsbutton ="//*[@id='actionsButton']";
	public static final String Add = "//*[@id='Summary_section_links_Add']/a";
	public static final String Account =" //*[@class='dx-item-content dx-menu-item-content']//span[text()='Account']";
	public static final String Loan = "//*[@class='dx-item dx-menu-item dx-menu-item-has-text']//span[text()='Loan']";
	public static final String ContractDate = "//*[@id='ChargeOff_Update-Update-Account_Detail-Contract_Date-text']";
	public static final String LoanDefinition = "//*[@id='Loan_Definition_Select-Loan_Definitions-Select_Loan_Definition-Loan_Definition-select']";
	public static final String Next =  "//*[@id='buttons_top']/span[2]/a";
	public static final String Inputsetname = "//*[@id='Staging Settings_category']/div[2]/table/tbody/tr/td[1]/a/img";
	public static final String Inputset = "//*[@id='refList']";
	public static final String Inputsetnameclose = "//*[@id='spectrumPanel']/div/div/div[1]/div/div[3]/div/div/div/div/i";
	public static final String OrginalAmount ="//*[@id='Loan_Update-Update-Loan_Detail-Original_Amount-text']";
	public static final String currentdateonheader ="//*[@id='currentDateHdr']";
	public static final String paymentamortization ="//*[@id='AddLoan_SHAW_SYSTEM-Add-Loan_Detail-PaymentAmortizationOverride']";
	public static final String paymentscheduleplus ="//*[@id='Payment Schedules_dynaAdd']";

	public static final String paymentschedule_StartDate="//*[@id='event.account.paymentSchedules[0].startingPaymentDate']";
	public static final String paymentschedule_frequency= "//*[@id='Account_Add-Loan_Account_Add-Payment_Schedules-Frequency-select']";
	public static final String paymentschedule_pyments ="//*[@id='event.account.paymentSchedules[0].numberOfPayments']";
	public static final String paymentschedule_Amount = "//*[@id='event.account.paymentSchedules[0].paymentAmount']";
	public static final String paymentschedule_type ="//*[@id='Payment_Schedule_Update-Update_Payment_Schedules-Payment_Schedules-Payment_Type-select']";
	public static final String done = "//*[@id='buttons_bottom']/span[3]/a";
	public static final String acccountnumber = "//*[@id='customer_hud_table']/tbody/tr[7]/td[2]/a";

	
	//process payment
	public static final String PPaccount = "//*[@class='dx-item-content dx-menu-item-content']//span[text()='Account']";
	public static final String PPaccounttasks = "//*[@class='dx-item-content dx-menu-item-content']//span[text()='Account Tasks']";
	public static final String PPaccountnumber = "//*[@id=\"Account_Tasks-Retrieve-Account_Retrieve-Account_Number-text\"]";
	public static final String PPactionbutton = "//*[@id=\"actionsButton\"]";
	public static final String PPtransactionentry = "//*[@class='dx-item-content dx-menu-item-content']//span[text()='Transaction Entry']";
	public static final String PPinputset = "//*[@id='inputSetName']";
	public static final String PPamount = "//*[@id='amount']";
	public static final String PPtype = "//*[@id='transactionCode']";
	public static final String PPprocess = "//*[@id='process']";
}
	