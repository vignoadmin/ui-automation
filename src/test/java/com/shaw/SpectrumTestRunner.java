package com.shaw;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import io.cucumber.junit.Cucumber;
import com.shaw.zephyr.JiraUpdate;
import com.shaw.framework.FrameworkConstants;

//RunnerSteps  for Spectrum
@RunWith(Cucumber.class)
@CucumberOptions(glue = { "com/shaw/StepDefs", }, features = "src/test/resources/features", plugin = {
		"html:target/cucumber-htmlreport", "json:target/cucumber-report.json",
		"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" }, tags = {
				"@SpectrumRegression" }, monochrome = true)
@Listeners(com.shaw.framework.FrameworkLibrary.class)
public class SpectrumTestRunner extends AbstractTestNGCucumberTests {
	
	@AfterClass
	public static void teardown() throws Exception {
		
		JiraUpdate j = new JiraUpdate();
		j.updateToJira("16350", "Unscheduled", "Spectrum","DEV", FrameworkConstants.tcResultsMap);
	}

	@BeforeClass
	public static void setup() throws Exception {
		
	}

	public static void cleanUp() throws Exception {
		
	}
}
