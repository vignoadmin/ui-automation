########################################################################
###############################Scenarios Start##########################
########################################################################  
Feature: Validate Spectrum functionality 

Background: Launch Application 
	Given Initializes the TestData 
	When User launches the "Spectrum" application 
	And user enters userid and password to login application 
	
	#-----------------------------Scenario 1--------------------------------# 
	#Scenario: Spectrum scenario
@CustomerSearch 
Scenario: Customer Search 
#Given User writes scenario name "AUTO_Spectrum_Scenario1" to log 
#When User launches the "Spectrum" application 
#And user enters userid and password to login application 
	And user clicks customer search button 
	Then user enters city name 
	And user clicks process button 
	
	#Scenario: Spectrum scenario
@AccountNumberSearch 
Scenario: Account Number search 
	Given User writes scenario name "AUTO_Spectrum_Scenario1" to log 
	When User launches the "Spectrum" application 
	And user enters userid and password to login application 
	And user clicks account search button 
	Then user selects "CHARGEOFF" as account type 
	And user clicks process button 
	
@TransactionEntry 
Scenario: Account Number search 
	Given User writes scenario name "AUTO_Spectrum_Scenario1" to log 
	When User launches the "Spectrum" application 
	And user enters userid and password to login application 
	And user selects Transaction Entry 
	
	
#@SpectrumRegression @AddCustomer 
#Scenario: Add customer 
#	Given User writes scenario name "AUTO_Spectrum_Scenario1" to log 
#	When User launches the "Spectrum" application 
#	And user enters userid and password to login application 
#	And User selects Customer from Dropdown 
#	And User clicks on Add 
#	And User clicks on Individual 
#	Then User Enters FirstName "Aelvin" 
#	And User Enters LastName as "Kames" 
#	And User Selects legal reporting type as "SSN" 
#	And User Enters SSN as "555121212" 
#	And User clicks on plus button in Address field 
#	Then User selects address type as "PRIMARY" 
#	And User Enters line1 as "Florida" 
#	And User Enters city as "Florida" 
#	And User selects state as "FL" 
#	And User Enters zipcode "12345" 
#	And User clicks on process button for adding information 
#	And User clicks on process button 
#	#And User checks Customer Care Number 

@SpectrumRegression @AddCustomer123
Scenario: Add customer
	Given User writes scenario name "AUTO_Spectrum_Scenario1" to log 
#	When User launches the "Spectrum" application 
#	And user enters userid and password to login application 
	And User selects Customer from Dropdown 
	And User clicks on Add 
	And User clicks on Individual 
	Then User Enters FirstName
	And User Enters LastName
	And User Selects legal reporting type as "SSN" 
	And User Enters SSN
	And User clicks on plus button in Address field 
	Then User selects address type as "PRIMARY" 
	And User Enters line1
	And User Enters city 
	And User selects state 
	And User Enters zipcode 
	And User clicks on process button for adding information 
	And User clicks on process button 
	#And User checks Customer Care Number 

	
@AddingloantoCustomer @SpectrumRegression
Scenario: Adding loan to customer 
	Given User writes scenario name "AUTO_Spectrum_Scenario1" to log 
#	When User launches the "Spectrum" application 
#	And user enters userid and password to login application 
	And User selects Customer from Dropdown 
	And User clicks on Customer Tasks 
	And User Enters the Customer Number 
	And User clicks on process button 
	Then User clicks on Action to add loan account 
	Then User Enters current System Date in contract Date 
	Then User selects from LoanDefinition Dropdown "REDSI360 - Retail Direct SI 30/360" 
	Then User Clicks on Next 
	Then User selects InputSetName Dropdown "ADDCL6" 
	Then User clicks on Close button 
	Then User Enters the Orginal Amount In Loan Detail section "30512.00" 
	Then User Validates the Accurual Rate 
	Then User selects from dropdown under Payment Amortization Override field "Yes" 
	And User clicks on plus button under payment schedule 
	Then User Enters date one month in the future from the Contract Date 
	Then User selects from the dropdown on the Frequency field selects "Monthly" 
	Then User Enters "60" to set Maturity Date in the Payments field 
	Then User Enters "586.94" in the Amount field 
	Then User selects "PRININTBH" from the dropdown on PaymentType 
	And User clicks on process button for adding information 
	Then User clicks on Done 
	And User checks Account Number 
	
	
@ProcessPayment @SpectrumRegression
Scenario: Process payment 
	Given User writes scenario name "AUTO_Spectrum_Scenario1" to log 
#	When User launches the "Spectrum" application 
#	And user enters userid and password to login application 
	Then User Selects Account from Dropdown 
	Then User Clicks Account Tasks 
	Then User Enters Accountnumber created in previous test in AccountNumber field 
	And User clicks on process button 
	Then User Clicks on Action Menu 
	Then User Clicks on Transaction Entry 
	Then User Selects "DIRECT" from Inputset under Staging settings 
	Then User Enters Amount in Transaction detail "101.00" 
	Then User Selects "PAYMENT" from Type under Staging Transaction detail 
	And user clicks process payment button 
	
	
	
	
@ReversingPayment @SpectrumRegression 
Scenario: Reversing payment 
	Given User writes scenario name "AUTO_Spectrum_Scenario1" to log 
#	When User launches the "Spectrum" application 
#	And user enters userid and password to login application 
	Then User Selects Account from Dropdown 
	Then User Clicks Account Tasks 
	Then User Enters Accountnumber created in previous test in AccountNumber field 
	And User clicks on process button 
	Then User Clicks on Action Menu 
	Then User Clicks on Transaction Entry 
	Then User Selects "DIRECT" from Inputset under Staging settings 
	Then User Enters Amount in Transaction detail "101.00" 
	Then User Selects "PAYMENT" from Type under Staging Transaction detail 
	And user clicks process payment button 
	
	
@Chargeoff @SpectrumRegression 
Scenario: ChargeOff 
	Given User writes scenario name "AUTO_Spectrum_Scenario1" to log 
#	When User launches the "Spectrum" application 
#	And user enters userid and password to login application 
	Then User Selects Account from Dropdown 
	Then User Clicks Account Tasks 
	Then User Enters Accountnumber created in previous test in AccountNumber field 
	And User clicks on process button 
	Then User Clicks on Action Menu 
	Then User Clicks on Transaction Entry 
	Then User Selects "DIRECT" from Inputset under Staging settings 
	Then User Enters Amount in Transaction detail "101.00" 
	Then User Selects "PAYMENT" from Type under Staging Transaction detail 
	And user clicks process payment button	